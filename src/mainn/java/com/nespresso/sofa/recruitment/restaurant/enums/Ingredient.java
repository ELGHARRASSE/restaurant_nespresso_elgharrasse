package com.nespresso.sofa.recruitment.restaurant.enums;

public enum Ingredient {
	
	MOZZARELLA,
	TOMATOES,
	OLIVE_OIL,
	SEA_SALT,
	PEPPER;

}
