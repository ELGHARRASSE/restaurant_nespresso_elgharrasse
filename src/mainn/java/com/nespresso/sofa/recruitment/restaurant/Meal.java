package com.nespresso.sofa.recruitment.restaurant;


public class Meal {

	private Ticket ticket;
	
	public Meal(Ticket ticket) {
		this.ticket = ticket;
	}

	public int servedDishes() {
		return ticket.calculateNumberOfDishes();
	}

	public String cookingDuration() {
		return String.valueOf(ticket.calculateCookingDuration());
	}

}
