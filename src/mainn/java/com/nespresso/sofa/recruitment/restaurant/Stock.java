package com.nespresso.sofa.recruitment.restaurant;

import java.util.Map;
import java.util.Map.Entry;

import com.nespresso.sofa.recruitment.restaurant.enums.Dish;
import com.nespresso.sofa.recruitment.restaurant.enums.Ingredient;

public class Stock {
	
	private Map<Ingredient,Integer> stock;
	
	public Stock(Map<Ingredient, Integer> stock) {
		super();
		this.stock = stock;
	}

	public boolean checkStock(Dish dish,int numberOfDishes) {
		for(Entry<Ingredient,Integer> entry : dish.recipe().entrySet()){
			int qtityInStock = stock.get(entry.getKey());
			int qtityShould = entry.getValue() * numberOfDishes;
			if(qtityInStock<qtityShould)
				return false;
		}
		return true;
	}

	public void updateStock(Dish dish, int numberOfDishes) {
		for(Entry<Ingredient,Integer> entry : dish.recipe().entrySet()){
			int qtityInStock = stock.get(entry.getKey());
			int qtityConsumed = entry.getValue() * numberOfDishes;
			stock.put(entry.getKey(),qtityInStock-qtityConsumed);	
		}
	}

}
