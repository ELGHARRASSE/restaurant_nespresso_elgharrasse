package com.nespresso.sofa.recruitment.restaurant.parser;

import com.nespresso.sofa.recruitment.restaurant.enums.Dish;
import com.nespresso.sofa.recruitment.restaurant.enums.Ingredient;

public class Parser {

	private static final String INGREDIENT_QTITY_SEPARATOR = " ";
	private static final String BALL_MEASURE = "balls";
	private static final String DISH_QTITY_SEPARATOR = " ";

	public static Ingredient parseIngredient(String ingredientRepresentation) {
		String ingredientSplitted[] = ingredientRepresentation.split(INGREDIENT_QTITY_SEPARATOR);
		StringBuilder builder = new StringBuilder();
		for(String prop : ingredientSplitted){
			if(!isNumeric(prop) && !isUnitOfmeasure(prop)){
				builder.append(prop.toUpperCase());
				builder.append("_");
			}
		}	
		return stringToIngredient(builder.deleteCharAt(builder.length() - 1).toString());
	}

	private static boolean isUnitOfmeasure(String prop) {
		return prop.equals(BALL_MEASURE);
	}

	public static int parseQuantity(String ingredientRepresentation) {
		String quantity = ingredientRepresentation.split(INGREDIENT_QTITY_SEPARATOR)[0];
		return isNumeric(quantity) ? Integer.parseInt(quantity) : Integer.MAX_VALUE;
	}

	public static boolean isNumeric(String subject){
		try  
		{  
			Integer.parseInt(subject);  
			return true;
		}  
		catch(NumberFormatException nfe)  
		{  
			return false;  
		}		
	}

	private static Ingredient stringToIngredient(String ingredient){
		return Ingredient.valueOf(ingredient);
	}

	public static Dish parseDish(String order) {
		StringBuilder builder = new StringBuilder();
		for(String prop : order.split(DISH_QTITY_SEPARATOR)){
			if(!isNumeric(prop)){
				builder.append(prop.toUpperCase());
				builder.append("_");
			}
		}	
		return stringToDish(builder.deleteCharAt(builder.length() - 1).toString());
	}

	private static Dish stringToDish(String dish) {
		return Dish.valueOf(dish);
	}

	public static int parseNumberOfDishes(String order) {
		return stringToInt(order.split(DISH_QTITY_SEPARATOR)[0]);
	}

	private static int stringToInt(String number) {
		return Integer.valueOf(number).intValue();
	}

}
