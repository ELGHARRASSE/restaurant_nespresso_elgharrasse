package com.nespresso.sofa.recruitment.restaurant;

import java.util.HashMap;
import java.util.Map;

import com.nespresso.sofa.recruitment.restaurant.enums.Ingredient;
import com.nespresso.sofa.recruitment.restaurant.parser.Parser;

public class Restaurant {

	private Stock stock;

	public Restaurant(String... ingredients ) {
		this.stock=createStock(ingredients);
		System.out.println(this.stock);
	}

	private Stock createStock(String[] ingredients) {
		Map<Ingredient, Integer> stock= new HashMap<Ingredient, Integer>();
		for(String ingredientRepresentation : ingredients){
			Ingredient ingredient = Parser.parseIngredient(ingredientRepresentation);
			int quantity = Parser.parseQuantity(ingredientRepresentation);
			stock.put(ingredient, quantity);
		}
		return new Stock(stock);
	}

	public Ticket order(String order){
		Ticket ticket = new Ticket();
		return ticket.and(order, stock);	
	}

	public Meal retrieve(Ticket ticket) {
		return new Meal(ticket);
	}

}
