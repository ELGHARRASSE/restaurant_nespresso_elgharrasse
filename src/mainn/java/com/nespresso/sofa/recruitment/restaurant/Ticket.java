package com.nespresso.sofa.recruitment.restaurant;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.nespresso.sofa.recruitment.restaurant.enums.Dish;
import com.nespresso.sofa.recruitment.restaurant.exception.UnavailableDishException;
import com.nespresso.sofa.recruitment.restaurant.parser.Parser;

public class Ticket {

	private Map<Dish,Integer> dishes = new HashMap<Dish, Integer>();

	private void addDish(Dish dish, int numberOfDishes){
		dishes.put(dish, numberOfDishes);
	}

	public int calculateNumberOfDishes() {
		int numberOfDishes = 0;
		for(int number : dishes.values()){
			numberOfDishes+=number;
		}
		return numberOfDishes;
	}

	public int calculateCookingDuration() {
		int totalDuration = 0;
		for(Entry<Dish, Integer> entry : dishes.entrySet()){
			int timeNeeded = entry.getKey().preparationTimeNeeded();
			int numberOfDishes = entry.getValue();
			totalDuration+=calculateDurationForDish(timeNeeded, numberOfDishes);
		}
		return totalDuration;
	}

	private int calculateDurationForDish(int timeNeeded, int numberOfDishes) {
		int duration = 0;
		int time = timeNeeded;
		for(int i=0; i<numberOfDishes; i++ ){
			duration+=time;
			if(time%2==0)
				time/=2;
		}
		return duration;
	}

	public Ticket and(String order,Stock stock) {
		Dish dish = Parser.parseDish(order);
		int numberOfDishes = Parser.parseNumberOfDishes(order);
		if(stock.checkStock(dish,numberOfDishes)){		
			addDish(dish, numberOfDishes);
			return this;
		}
		else
			throw new UnavailableDishException();

	}

}
