package com.nespresso.sofa.recruitment.restaurant.enums;

import java.util.HashMap;
import java.util.Map;

public enum Dish {

	TOMATO_MOZZARELLA_SALAD {
		@Override
		public Map<Ingredient, Integer> recipe() {
			Map<Ingredient, Integer> recipe = new HashMap<Ingredient, Integer>();
			recipe.put(Ingredient.MOZZARELLA,1);
			recipe.put(Ingredient.TOMATOES,2);
			recipe.put(Ingredient.OLIVE_OIL,1);
			return recipe;
		}

		@Override
		public int preparationTimeNeeded() {
			return 6;
		}
	};
	
	public abstract Map<Ingredient,Integer> recipe();
	public abstract int preparationTimeNeeded();

}
